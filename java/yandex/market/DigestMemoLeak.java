package yandex.market;

public class DigestMemoLeak extends Digest {

    byte inc;


    @Override
    protected byte[] doDigest(byte[] input) {
        byte[] key = new byte[1];
        inc = (byte) (inc + 0x1);
        key[0] = inc;
        return key;
    }

    public static void main(String[] args) throws InterruptedException {
        DigestMemoLeak ext = new DigestMemoLeak();

        byte[] key1 = {1};
        byte[] key2 = {1};

        ext.digest(key1);


        System.out.println(ext.digest(key2)[0]);
        System.out.println(ext.digest(key1)[0]);
        System.out.println(ext.digest(key1)[0]);
        System.out.println(ext.digest(key2)[0]);


    }
}

package yandex.market;

import java.util.HashMap;
import java.util.Map;


/*
    Перечислите все проблемы, которые вы видите в данном коде:
 */

public abstract class Digest {

    private Map<byte[], byte[]> cache = new HashMap<byte[], byte[]>();

    public byte[] digest(byte[] input) {

        byte[] result = cache.get(input);
        if (result == null) {
            synchronized (cache) {
                result = cache.get(input);
                if (result == null) {
                    result = doDigest(input);
                    cache.put(input, result);
                }
            }
        }
        return result;
    }

    protected abstract byte[] doDigest(byte[] input);
}

/*
    1. Утечка памяти
    Из-за использования byte[] в качестве ключа будет утекать память.
    Так-как массив использует hashCode и equals Object то byte[] key1 = {1} и byte[] key2 = {1} всегда будут не равны и иметь разные хеш-коды.
    Отсюда следует, что значения будут храниться не по хешу от данных массива, а по случайным числам (в зависимости от настроек JVM).

    2. Потенциальный дедлок
    Можно написать такую реализацию метода doDigest который спровоцирует дедлок
    Например:
    @Override
    protected byte[] doDigest(byte[] input)  {
        byte[] key = {1};
        Thread deadLock = new Thread( () ->  this.digest(key) ,"DeadLock");
        deadLock.start();
        try {
            deadLock.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return key;
    }

    3. Низкая многопоточная производительность
    Так как блокировка происходит на уровне всего Map то при использование в многопоточной среде будет тратится много времени на ожидание реализации лока.
    Это делает бессмысленным использование HashMap так-как он становится по сути Hashtable.
    Вместо этого лучше использовать ConcurrentHashMap у которого блокировки реализованы на уровне корзин (buckets).




 */
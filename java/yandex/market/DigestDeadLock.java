package yandex.market;

public class DigestDeadLock extends Digest {


    @Override
    protected byte[] doDigest(byte[] input) {
        byte[] key = {1};
        Thread deadLock = new Thread(() -> this.digest(key), "DeadLock");
        deadLock.start();
        try {
            deadLock.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return key;
    }

    public static void main(String[] args) throws InterruptedException {
        DigestDeadLock test = new DigestDeadLock();

        byte[] key = {1};

        test.digest(key);
    }
}
